package com.shpp.pzobenko;

import com.shpp.pzobenko.activemqfolder.ActiveMQFactory;
import com.shpp.pzobenko.activemqfolder.Consumer;
import com.shpp.pzobenko.activemqfolder.Producer;
import com.shpp.pzobenko.validator.Validate;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.jms.JMSException;
import java.io.IOException;

/**
 * Get num of messages from console arguments, create ActiveMQ connection,create messages and send
 * the messages, than get messages from ActiveMQ broker and validate it on two different csv files.
 */
public class Level3 {
    //Logger
    private static final Logger log = LoggerFactory.getLogger(Level3.class);

    /**
     * Load properties and parse first console argument on Properties class.
     * @param args Arguments from console.
     * @throws JMSException JMS exception.
     * @throws IOException IOE exception.
     */
    public static void main(String[] args) throws JMSException, IOException {
        Properties prop = new Properties(args[0]);
        log.debug("Properties loads successful.");
        final ActiveMQConnectionFactory connectionFactory =
                ActiveMQFactory.createActiveMQConnectionFactory(prop.getBrokerUrl());
        log.debug("Connection factory was created.");
        Producer.sendMessage(connectionFactory, prop.getNameOfQueue(), prop.getTimeOnSeconds(),
                prop.getNOfMessages(), prop.getPoisonPillMessage());
        Validate.validateMessages(Consumer.receiveMessage(connectionFactory, prop.getNameOfQueue(),
                prop.getPoisonPillMessage()));
    }
}
