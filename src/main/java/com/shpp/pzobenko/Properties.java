package com.shpp.pzobenko;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Load properties and element from args.
 */
public class Properties {

    // Specify the connection parameters and parameters for messages.
    private String nameOfQueue;
    private String brokerUrl;
    private String poisonPillMessage;
    private int nOfMessages;
    private long timeOnSeconds;

    /**
     * logger
     */
    public static final Logger log = LoggerFactory.getLogger(Properties.class);

    /**
     * Constructor.
     * @param n Value from console which have the num of messages which need to create.
     */
    public Properties(String n) {
        loadProperties(n);
    }

    /**
     * Load value from properties file.
     * @param nOfMessages The number of messages which need to generate and send to broker.
     */
    private void loadProperties(String nOfMessages) {
        java.util.Properties properties = new java.util.Properties();
        try {
            properties.load(Level3.class.getClassLoader().getResourceAsStream("prop.properties"));
            brokerUrl = properties.getProperty("WIRE_LEVEL_ENDPOINT");
            nameOfQueue = properties.getProperty("NAME_OF_QUEUE");
            poisonPillMessage = properties.getProperty("POISON_PILL_MESSAGES");
            log.debug("Properties from file prop.properties loads successful.");
        } catch (IOException e) {
            log.error("Something go wrong...", e);
        }
        try {
            timeOnSeconds = Long.parseLong(properties.getProperty("POISON_PILL_SECONDS"));
            log.debug("Properties from file prop.properties the Poison pill seconds loads successful.");
        } catch (NumberFormatException e) {
            log.error("Boom!", e);
        }
        try {
            this.nOfMessages = Integer.parseInt(nOfMessages);
            log.debug("Properties from file arguments line the N of messages loads successful.");
        } catch (NumberFormatException e) {
            log.error("Boom!",  e);
        }
        log.info("properties loads successful.");
    }

    /**
     * Give name of queue.
     * @return  Name of queue.
     */
    public String getNameOfQueue() {
        log.debug("getNameOfQueue.");
        return nameOfQueue;
    }

    /**
     * Give broker URL.
     * @return Broker URL.
     */
    public String getBrokerUrl() {
        log.debug("getWireLevelEndpoint.");
        return brokerUrl;
    }

    /**
     * Give poison pill message.
     * @return poison pill message.
     */
    public String getPoisonPillMessage() {
        log.debug("getPoisonPillMessage.");
        return poisonPillMessage;
    }

    /**
     * Give number of messages.
     * @return number of messages.
     */
    public int getNOfMessages() {
        log.debug("getNOfMessages.");
        return nOfMessages;
    }

    /**
     * Give time which need to stop steam which generate pojo`s
     * @return time on seconds.
     */
    public long getTimeOnSeconds() {
        log.debug("getTimeOnSeconds.");
        return timeOnSeconds;
    }
}