package com.shpp.pzobenko.activemqfolder;

import com.shpp.pzobenko.Constants;
import org.apache.activemq.ActiveMQConnectionFactory;

import java.util.Arrays;


public class ActiveMQFactory {
    /**
     * This constructor has to be empty.
     */
    private ActiveMQFactory() {
    }

    /**
     * Creating ActiveMQ connection factory to connect producer and consumer letter
     * @param brokerUrl url of our ActiveMQ broker.
     * @return connection factory
     */
    public static ActiveMQConnectionFactory createActiveMQConnectionFactory(String brokerUrl) {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerUrl);
        connectionFactory.setTrustedPackages(Arrays.asList("com.shpp.pzobenko","com.shpp.pzobenko.pojo"));
        // Pass the username and password.
        connectionFactory.setUserName(Constants.ACTIVE_MQ_USERNAME);
        connectionFactory.setPassword(Constants.ACTIVE_MQ_PASSWORD);
        return connectionFactory;
    }
}
