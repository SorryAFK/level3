package com.shpp.pzobenko.activemqfolder;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.shpp.pzobenko.pojo.POJO;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.util.ArrayList;
import java.util.List;

public class Consumer {

    private static final Logger log = LoggerFactory.getLogger(Consumer.class);

    private Consumer() {
    }

    public static List<POJO>
    receiveMessage(ActiveMQConnectionFactory connectionFactory, String nameOfQueue, String poisonPill) throws JMSException, JsonProcessingException {
        // Establish a connection for the consumer.
        // Note: Consumers should not use PooledConnectionFactory.
        final Connection consumerConnection = connectionFactory.createConnection();
        consumerConnection.start();

        // Create a session.
        final Session consumerSession = consumerConnection
                .createSession(false, Session.AUTO_ACKNOWLEDGE);

        // Create a queue named "MyQueue".
        final Destination consumerDestination = consumerSession
                .createQueue(nameOfQueue);

        // Create a message consumer from the session to the queue.
        final MessageConsumer consumer = consumerSession
                .createConsumer(consumerDestination);

        // Receive the message when it arrives.
        String consumerTextMessage;
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        ArrayList<POJO> pojoList = new ArrayList<>();
        log.info("Consumer start read messages.");
        while (true) {
            consumerTextMessage = ((TextMessage) consumer.receive()).getText();
            if (consumerTextMessage.equals(poisonPill)) {
                log.info("Consumer stopped.");
                break;
            }
            log.debug(consumerTextMessage);
            pojoList.add(mapper.readValue(consumerTextMessage, POJO.class));
        }
        log.info("Message received successful.{}", pojoList.size());
        // Clean up the consumer.
        consumer.close();
        consumerSession.close();
        consumerConnection.close();
        return pojoList;
    }
}
