package com.shpp.pzobenko.activemqfolder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.shpp.pzobenko.pojo.GeneratePOJO;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.time.LocalDateTime;
import java.util.stream.IntStream;


public class Producer {

    public static final Logger log = LoggerFactory.getLogger(Producer.class);

    private Producer() {
    }

    public static void
    sendMessage(ActiveMQConnectionFactory connectionFactory, String nameOfQueue, long seconds, int numOfMessages,
                String poisonPill)
            throws JMSException {
        // Establish a connection for the producer.
        final Connection producerConnection = createConnectionWithActiveMQ(connectionFactory);
        producerConnection.start();
        // Create a session.
        final Session producerSession = producerCreateConnection(producerConnection);
        // Create a queue named "MyQueue".
        final Destination producerDestination = destination(producerSession, nameOfQueue);
        // Create a producer from the session to the queue.
        final MessageProducer producer = sessionProducer(producerSession, producerDestination);
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        LocalDateTime theEndTime = LocalDateTime.now().plusSeconds(seconds);

        log.debug("start sending messages at{}", LocalDateTime.now().getSecond());
        IntStream.range(0, numOfMessages).takeWhile(i -> LocalDateTime.now().getSecond()
                <= theEndTime.getSecond()).forEach(i -> {
            try {
                producer.send(producerSession.createTextMessage(mapper.writeValueAsString(
                        GeneratePOJO.generate(numOfMessages))));
            } catch (JMSException | JsonProcessingException e) {
                log.error("Boom!", e);
            }
        });
        log.debug("finish sending messages at:" + theEndTime.getSecond());
        producer.send(producerSession.createTextMessage(poisonPill));
        log.info("Messages sends.");
        // Clean up the producer.
        producer.close();
        log.debug("Producer is closed");
        producerSession.close();
        log.debug("Session is closed");
        producerConnection.close();
        log.debug("Connection is closed");
    }

    public static Connection createConnectionWithActiveMQ(ActiveMQConnectionFactory connectionFactory)
            throws JMSException {
        return connectionFactory.createConnection();
    }

    public static Session producerCreateConnection(Connection producerConnection) throws JMSException {
        return producerConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }

    public static Destination destination(Session producerSession, String nameOfQueue) throws JMSException {
        return producerSession.createQueue(nameOfQueue);
    }

    public static MessageProducer sessionProducer(Session producerSession, Destination producerDestination)
            throws JMSException {
        return producerSession.createProducer(producerDestination);
    }
}