package com.shpp.pzobenko.pojo;


import com.shpp.pzobenko.validator.havea.HaveAa;
import com.shpp.pzobenko.validator.lengthlessthan7.LengthLessThan7;

import javax.validation.constraints.Min;
import java.time.LocalDateTime;

public class POJO {
    @HaveAa()
    @LengthLessThan7()
    private String name;

    @Min(value = 10,message = "The column 'count' have less than 10 value.")
    private int count;

    private LocalDateTime time;

    @SuppressWarnings("unused")
    public POJO() {
    }

    public POJO(String name, int count, LocalDateTime time) {
        this.count = count;
        this.time = time;
        this.name = name;
    }

    @SuppressWarnings("unused")
    public void setName(String name) {
        this.name = name;
    }

    @SuppressWarnings("unused")
    public void setCount(int count) {
        this.count = count;
    }

    @SuppressWarnings("unused")
    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @SuppressWarnings("unused")
    public String getName() {
        return name;
    }

    @SuppressWarnings("unused")
    public int getCount() {
        return count;
    }

    @SuppressWarnings("unused")
    public LocalDateTime getTime() {
        return time;
    }


    public String toString() {
        return "POJO{" +
                "name='" + name + '\'' +
                ", count=" + count +
                ", time=" + time +
                '}';
    }
}