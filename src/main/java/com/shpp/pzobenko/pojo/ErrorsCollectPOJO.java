package com.shpp.pzobenko.pojo;

import java.util.ArrayList;

public class ErrorsCollectPOJO {

    private final ArrayList<String> errors = new ArrayList<>();

    public ErrorsCollectPOJO() {
    //This method is empty, say 'Thank for sonarLint...
    }

    @SuppressWarnings("unused")
    public String[] getErrors() {
        String[] strError = new String[errors.size()];
        return errors.toArray(strError);
    }

    public void setErrors(String errors) {
        this.errors.add(errors);
    }

    public void clear(){
        errors.clear();
    }

    @Override
    public String toString() {
        return "ErrorsCollectPOJO{" +
                "errors=" + errors +
                '}';
    }

    public boolean isEmpty(){
        return errors.isEmpty();
    }
}