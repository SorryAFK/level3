package com.shpp.pzobenko.pojo;

import com.shpp.pzobenko.Constants;

import java.time.LocalDateTime;
import java.util.concurrent.ThreadLocalRandom;

public class GeneratePOJO {

    private GeneratePOJO() {
    }

    public static POJO generate(int max) {
        StringBuilder name = new StringBuilder();
        for (int i = 1; i < ThreadLocalRandom.current().nextInt(Constants.NAME_MAX_LENGTH); i++) {
            name.append(Constants.getLetters()[ThreadLocalRandom.current().nextInt(Constants.getLetters().length)]);
        }
        return new POJO(name.toString(), ThreadLocalRandom.current().nextInt(max), LocalDateTime.now());
    }
}
