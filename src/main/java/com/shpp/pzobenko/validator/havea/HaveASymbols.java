package com.shpp.pzobenko.validator.havea;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class HaveASymbols implements ConstraintValidator<HaveAa,String> {
    @Override
    public void initialize(HaveAa constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s.contains("A") || s.contains("a");
    }
}