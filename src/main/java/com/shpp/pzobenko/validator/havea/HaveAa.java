package com.shpp.pzobenko.validator.havea;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = HaveASymbols.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface HaveAa {
    String message() default "The column 'name' don`t have 'a' or 'A' symbols";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
