package com.shpp.pzobenko.validator.lengthlessthan7;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LengthLessThan7Class implements ConstraintValidator<LengthLessThan7,String> {
    @Override
    public void initialize(LengthLessThan7 constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s.length()>=7;
    }
}