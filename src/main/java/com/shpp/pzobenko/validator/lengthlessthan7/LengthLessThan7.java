package com.shpp.pzobenko.validator.lengthlessthan7;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = LengthLessThan7Class.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LengthLessThan7 {
    String message() default "The column 'name' length less than 7";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
