package com.shpp.pzobenko.validator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;
import com.shpp.pzobenko.pojo.ErrorsCollectPOJO;
import com.shpp.pzobenko.pojo.POJO;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Validate {
    /**
     * need to be empty
     */
    private Validate() {
    }

    public static void validateMessages(List<POJO> pojoList) throws IOException {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        List<String[]> errorsForOutput = new ArrayList<>();
        errorsForOutput.add(new String[]{"name", "count", "time", "error"});
        String errorMessage;

        List<String[]> validForOutput = new ArrayList<>();
        validForOutput.add(new String[]{"name", "count"});

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        ErrorsCollectPOJO errorsCollectPOJO = new ErrorsCollectPOJO();
        for (POJO pojo : pojoList) {
            Set<ConstraintViolation<POJO>> constraintViolations = validator.validate(pojo);
            errorMessage = constraintViolations.toString();
            if (errorMessage.contains("The column 'name' length less than 7") ||
                    errorMessage.contains("The column 'name' don`t have 'a' or 'A' symbols") ||
                    errorMessage.contains("{javax.validation.constraints.Min.message}")) {
                for(ConstraintViolation<POJO> pojoConstraintViolation : constraintViolations){
                   errorsCollectPOJO.setErrors(pojoConstraintViolation.getMessage());
                }
                errorsForOutput.add(new String[]{pojo.getName(), pojo.getCount() + "", pojo.getTime() + "",
                        mapper.writeValueAsString(errorsCollectPOJO)});
                errorsCollectPOJO.clear();
            } else {
                validForOutput.add(new String[]{pojo.getName(), pojo.getCount() + ""});
            }
        }

        File errorsOutputFile = new File("ErrorsFile.csv");
        FileWriter outputFile = new FileWriter(errorsOutputFile);
        CSVWriter csvWriter = new CSVWriter(outputFile
                , ';',
                ICSVWriter.NO_QUOTE_CHARACTER,
                ICSVWriter.DEFAULT_ESCAPE_CHARACTER,
                ICSVWriter.DEFAULT_LINE_END);
        csvWriter.writeAll(errorsForOutput);
        csvWriter.close();

        File validOneOutputFile = new File("ValidFile.csv");
        outputFile = new FileWriter(validOneOutputFile);
        csvWriter = new CSVWriter(outputFile
                , ';',
                ICSVWriter.NO_QUOTE_CHARACTER,
                ICSVWriter.DEFAULT_ESCAPE_CHARACTER,
                ICSVWriter.DEFAULT_LINE_END);
        csvWriter.writeAll(validForOutput);
        csvWriter.close();
    }

}
