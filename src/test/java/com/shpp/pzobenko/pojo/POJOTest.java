package com.shpp.pzobenko.pojo;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class POJOTest {

    POJO pojo = new POJO();

    @Test
    void setName() {
        String actual = "Name";
        pojo.setName("Name");
        assertEquals(actual, pojo.getName());
    }

    @Test
    void setCount() {
        int actual = 11;
        pojo.setCount(11);
        assertEquals(actual, pojo.getCount());
    }

    @Test
    void setTime() {
        LocalDateTime time = LocalDateTime.now();
        pojo.setTime(time);
        assertEquals(time, pojo.getTime());
    }

    @Test
    void testToString() {
        String name = "SomethingName";
        int count = 6969;
        LocalDateTime time = LocalDateTime.now();
        String str = "POJO{" +
                "name='" + name + '\'' +
                ", count=" + count +
                ", time=" + time +
                '}';
        POJO pojo = new POJO(name, count, time);
        assertEquals(str,pojo.toString());
    }
}