package com.shpp.pzobenko.pojo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ErrorsCollectPOJOTest {
    String error0 = "error0";
    String error1 = "error1";
    String error2 = "error2";


    @Test
    void setErrors() {
        ErrorsCollectPOJO collectPOJO = new ErrorsCollectPOJO();
        collectPOJO.setErrors(error0);
        collectPOJO.setErrors(error1);
        collectPOJO.setErrors(error2);
        String expected = "ErrorsCollectPOJO{" +
                "errors=[" + error0 + ", " + error1 + ", " + error2 +
                "]}";
        assertEquals(expected, collectPOJO.toString());
    }

    @Test
    void clear() {
        ErrorsCollectPOJO collectPOJO = new ErrorsCollectPOJO();
        collectPOJO.setErrors(error0);
        collectPOJO.setErrors(error1);
        collectPOJO.setErrors(error2);
        collectPOJO.clear();
        assertTrue(collectPOJO.isEmpty());
    }
}