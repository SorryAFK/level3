package com.shpp.pzobenko;

import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class PropertiesTest {
    Properties prop = new Properties("18");

    @Test
    void getNameOfQueue() {
        String expected = "MaQueue";
        assertEquals(expected, prop.getNameOfQueue());
    }

    @Test
    void getBrokerUrl() {
        String expected = "ssl://b-49a46bdb-1088-4f41-b63e-b10a59d3cb9c-1.mq.eu-central-1.amazonaws.com:61617";
        assertEquals(expected, prop.getBrokerUrl());
    }

    @Test
    void getPoisonPillMessage() {
        String expected = "YOU_DIE_HAHA";
        assertEquals(expected, prop.getPoisonPillMessage());
    }

    @Test
    void getNOfMessages() {
        int expected = 18;
        assertEquals(expected, prop.getNOfMessages());
    }

    @Test
    void getTimeOnSeconds() {
        long expected = 4;
        assertEquals(expected, prop.getTimeOnSeconds());
    }
}