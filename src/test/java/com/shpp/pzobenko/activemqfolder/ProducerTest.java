package com.shpp.pzobenko.activemqfolder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.shpp.pzobenko.pojo.POJO;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.jupiter.api.Test;

import javax.jms.*;


import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class ProducerTest {

    private ActiveMQConnectionFactory factory() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("ssl://b-49a46bdb-1088-4f41-b63e-b10a59d3cb9c-1.mq.eu-central-1.amazonaws.com:61617");
        connectionFactory.setUserName("MaBrokerBaby");
        connectionFactory.setPassword("1BmmmmmmX2003");
        return connectionFactory;
    }

    private Connection connection() throws JMSException {
        return factory().createConnection();
    }

    private Session session() throws JMSException {
        return connection().createSession(false, Session.AUTO_ACKNOWLEDGE);
    }

    private Destination destination() throws JMSException {
        return session().createQueue("EmptyOnlyForTestPlsDeleteMe");
    }

    private MessageProducer messageProducer() throws JMSException {
        return session().createProducer(destination());
    }

    @Test
    void createConnectionWithActiveMQ() throws JMSException {
        Connection actual = Producer.createConnectionWithActiveMQ(factory());
        assertEquals(connection().getClientID(), actual.getClientID());
        assertEquals(connection().getMetaData().getJMSProviderName(), actual.getMetaData().getJMSProviderName());
        connection().close();
        actual.close();
    }

    @Test
    void producerCreateConnection() throws JMSException {
        Session actual = Producer.producerCreateConnection(connection());
        assertEquals(destination(), actual.createQueue("EmptyOnlyForTestPlsDeleteMe"));
        actual.close();
    }

    @Test
    void destinationProducerTest() throws JMSException {
        Destination actual = Producer.destination(Producer.producerCreateConnection(connection()), "EmptyOnlyForTestPlsDeleteMe");
        assertEquals(destination(), actual);
    }

    @Test
    void sessionProducer() throws JMSException {
        MessageProducer actual = Producer.sessionProducer(session(), destination());
        assertEquals(messageProducer().getDestination(),actual.getDestination());
        actual.close();
        messageProducer().close();
    }

    @Test
    void sendMessage() throws JMSException, JsonProcessingException {
        Connection connection = factory().createConnection();
        connection.start();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Destination destinationForProducer = session.createQueue("test1");
        Destination destinationExpected = session.createQueue("testExpected");
        MessageProducer expected = session.createProducer(destinationExpected);
        MessageProducer actual = Producer.sessionProducer(session, destinationForProducer);
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        POJO pojo= new POJO("test",1, LocalDateTime.now());
        actual.send(session.createTextMessage(mapper.writeValueAsString(pojo)));
        expected.send(session.createTextMessage(mapper.writeValueAsString(pojo)));
        MessageConsumer consumerExpected = session.createConsumer(destinationExpected);
        MessageConsumer consumerActual = session.createConsumer(destinationForProducer);
        assertEquals(((TextMessage) consumerExpected.receive()).getText(),((TextMessage) consumerActual.receive()).getText());
        consumerExpected.close();
        consumerActual.close();
        expected.close();
        actual.close();
        session.close();
        connection.close();
    }
}