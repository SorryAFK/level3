package com.shpp.pzobenko.activemqfolder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.shpp.pzobenko.pojo.POJO;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.jupiter.api.Test;

import javax.jms.*;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ConsumerTest {

    private ActiveMQConnectionFactory factory() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("ssl://b-49a46bdb-1088-4f41-b63e-b10a59d3cb9c-1.mq.eu-central-1.amazonaws.com:61617");
        connectionFactory.setUserName("MaBrokerBaby");
        connectionFactory.setPassword("1BmmmmmmX2003");
        return connectionFactory;
    }

    @Test
    void receiveMessage() throws JMSException, JsonProcessingException {
        String nameOfQueue = "testExpected";
        String poisonPill = "die";
        Connection connection = factory().createConnection();
        connection.start();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Destination destinationExpected = session.createQueue(nameOfQueue);
        MessageProducer expected = session.createProducer(destinationExpected);
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        POJO pojo= new POJO("test",1, LocalDateTime.now());
        expected.send(session.createTextMessage(mapper.writeValueAsString(pojo)));
        expected.send(session.createTextMessage(poisonPill));
        List<POJO> actualList = Consumer.receiveMessage(factory(),nameOfQueue,poisonPill);
        assertEquals(pojo.toString(), actualList.get(0).toString());
        expected.close();
        session.close();
        connection.close();
    }
}