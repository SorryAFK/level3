package com.shpp.pzobenko.activemqfolder;

import com.shpp.pzobenko.Constants;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ActiveMQFactoryTest {

    @Test
    void createActiveMQConnectionFactory() {
        String url = "ssl://b-fe2b2d1e-725b-4fc6-b2e4-26dfa5976b3d-1.mq.eu-central-1.amazonaws.com:61617";
        ActiveMQConnectionFactory expected = new ActiveMQConnectionFactory(url);
        expected.setUserName(Constants.ACTIVE_MQ_USERNAME);
        expected.setPassword(Constants.ACTIVE_MQ_PASSWORD);
        ActiveMQConnectionFactory actual =ActiveMQFactory.createActiveMQConnectionFactory(url);
        assertEquals(expected.getBrokerURL(),actual.getBrokerURL());
        assertEquals(expected.getUserName(),actual.getUserName());
        assertEquals(expected.getClientID(),actual.getClientID());
    }
    @Test
    void createActiveMQConnectionFactoryButWrongUrl() {
        String url = "haha";
        ActiveMQConnectionFactory expected = new ActiveMQConnectionFactory(url);
        expected.setUserName(Constants.ACTIVE_MQ_USERNAME);
        expected.setPassword(Constants.ACTIVE_MQ_PASSWORD);
        assertEquals(expected.getBrokerURL(),ActiveMQFactory.createActiveMQConnectionFactory(url).getBrokerURL());
    }
}