package com.shpp.pzobenko.validator;

import com.shpp.pzobenko.pojo.POJO;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class ValidateTest {

    @Test
    void validateMessages() throws IOException {
        POJO pojo = new POJO("wrongName", 11, LocalDateTime.now());
        List<POJO> list = new ArrayList<>();
        list.add(pojo);
        Validate.validateMessages(list);
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader("ValidFile.csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        assertNotNull(bufferedReader);
    }

    @Test
    void haveA() {
        POJO pojo = new POJO("wrongNme", 10, LocalDateTime.now());
        String expected = "The column 'name' don`t have 'a' or 'A' symbols";
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<POJO>> constraintViolations = validator.validate(pojo);
        assertEquals(expected, constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    void lengthLessThan7() {
        POJO pojo = new POJO("wrongA", 10, LocalDateTime.now());
        String expected = "The column 'name' length less than 7";
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<POJO>> constraintViolations = validator.validate(pojo);
        assertEquals(expected, constraintViolations.iterator().next().getMessageTemplate());
    }
}